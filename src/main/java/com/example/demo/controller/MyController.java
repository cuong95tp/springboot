package com.example.demo.controller;

import com.example.demo.models.Dress;
import com.example.demo.models.User;
import com.example.demo.pojo.Book;
import com.example.demo.repositories.DressRepository;
import com.example.demo.repositories.UserRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;


@RestController
public class MyController {
    @GetMapping("/sum")
    Integer sum(@RequestParam("a") Integer one, @RequestParam("b") Integer two) {
        return one + two;
    }

    @PostMapping("/save-book")
    String saveBook(@RequestBody Book book) {
        return book.getName();
    }

    @Autowired
    UserRepository userRepository;
    @PostMapping("/user")
    void test(@RequestBody User user){
        userRepository.save(user);
    }

    @Autowired
    DressRepository dressRepository;
    @PostMapping("/dress")
    void createNewDress(@RequestBody Dress dress){
        dressRepository.save(dress);
    }
}
