package com.example.demo.models;

import lombok.Data;
import lombok.Getter;
import lombok.Setter;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;

@Entity
@Data
@Setter
@Getter
public class Dress {
    @Id
    @GeneratedValue
    private Long idDress;
    private String name;
    private String size;
    private Long price;
    private String color;
    private int quantum;
}
